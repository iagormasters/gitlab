package br.com.automation.karate.utils;

import java.security.SecureRandom;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class Utils {
    /**
     * Inmetrics<BR>
     * <p>
     * Create timestamp
     * <p>
     *
     * @author Inmetrics<BR>
     */
    public static long getCurrentTimestamp() {
        return new Timestamp((System.currentTimeMillis() / 1000)).getTime();
    }

    /**
     * Inmetrics<BR>
     * <p>
     *
     * @author Inmetrics<BR>
     */
    public static long getCurrentTimestampPlusMinutes(int min) {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MINUTE, min);
        return new Timestamp(c.getTimeInMillis() / 1000).getTime();
    }

    /**
     * Inmetrics<BR>
     * <p>
     * Get Date Now Format
     *
     * @author Inmetrics<BR>
     * @since 24 de nov de 2020 16:20:20
     */
    public static String getDateNowFormat(String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(new Date());
    }

    /**
     * Inmetrics<BR>
     * <p>
     * Get Format DateTime
     *
     * @author Inmetrics<BR>
     */
    public static String formatDateTime(String dateTime, String format) throws ParseException {
        SimpleDateFormat formatt = new SimpleDateFormat("yyyy-MM-dd");
        Date dateFormat = formatt.parse(dateTime);
        SimpleDateFormat parser = new SimpleDateFormat(format);
        return parser.format(dateFormat);
    }
}
