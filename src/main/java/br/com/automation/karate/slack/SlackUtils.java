package br.com.automation.karate.slack;

import com.intuit.karate.Results;
import com.intuit.karate.core.ScenarioResult;
import com.intuit.karate.core.Step;
import com.slack.api.Slack;
import com.slack.api.methods.SlackApiException;
import com.slack.api.methods.request.chat.ChatPostMessageRequest;
import com.slack.api.methods.response.chat.ChatPostMessageResponse;
import com.slack.api.model.Field;
import com.slack.api.model.block.ActionsBlock;
import com.slack.api.model.block.LayoutBlock;
import com.slack.api.model.block.element.BlockElement;
import com.slack.api.model.block.element.ButtonElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static com.slack.api.model.Attachments.asAttachments;
import static com.slack.api.model.Attachments.attachment;
import static com.slack.api.model.block.Blocks.divider;
import static com.slack.api.model.block.Blocks.header;
import static com.slack.api.model.block.composition.BlockCompositions.plainText;

public class SlackUtils {

    private static final Slack slack = Slack.getInstance();
    private static final Logger log = LoggerFactory.getLogger(SlackUtils.class);

    public static void sendMessage(Results results, boolean wasItSuccessful, String slackChannel, String slackToken, String tags, String env) {
        try {

            // Build a request object
            ChatPostMessageRequest.ChatPostMessageRequestBuilder request = ChatPostMessageRequest.builder();

            request.channel(slackChannel);

            List<LayoutBlock> blocks = new ArrayList<>();
            blocks.add(divider());
            blocks.add(header(headerBlockBuilder -> headerBlockBuilder.text(plainText("automation-api-karate-picpay:"))));

            List<BlockElement> actionsElementList = new ArrayList<>();

            String jobUrl = getGitlabJobUrl();
            String artifactsUrl = getGitlabArtifactUrl(jobUrl);

            if (artifactsUrl != null) {
                ButtonElement artifactsButton = new ButtonElement();
                artifactsButton.setText(plainText(pt -> pt.emoji(true).text("Test Artifacts")));
                artifactsButton.setUrl(artifactsUrl);
                actionsElementList.add(artifactsButton);
            }

            if (jobUrl != null) {
                ButtonElement gitlabButton = new ButtonElement();
                gitlabButton.setText(plainText(pt -> pt.emoji(true).text("Gitlab")));
                gitlabButton.setUrl(jobUrl);
                actionsElementList.add(gitlabButton);
            }

            if (actionsElementList.size() > 0) {
                ActionsBlock actionsBlock = new ActionsBlock();
                actionsBlock.setElements(actionsElementList);
                blocks.add(actionsBlock);
            }

            request.blocks(blocks);

            request.attachments(asAttachments(
                    attachment(a -> a
                            .color(getColor(wasItSuccessful))
                            .text(getTitle(results, tags, env))
                            .fields(getErrors(results, wasItSuccessful))
                    )
            ));

            ChatPostMessageResponse response = slack.methods(slackToken).chatPostMessage(request.build());

            if (response.getError() != null) {
                log.error("SLACK RESPONSE ERROR: " + response.getError());
            }

        } catch (IOException | SlackApiException e) {
            log.error("Failed sending report to Slack");
            log.error(e.toString());
        }
    }

    private static List<Field> getErrors(Results results, boolean wasItSuccessful) {
        List<Field> blocks = new ArrayList<>();

        if (wasItSuccessful) {
            Field message = new Field();
            message.setValue("*All tests succeeded!*");
            blocks.add(message);
        } else {
            for (ScenarioResult scenarioResult : results.getScenarioResults()) {
                Field error = new Field();
                if (scenarioResult.isFailed()) {
                    String feature = scenarioResult.getScenario().getFeature().getName();
                    String scenario = scenarioResult.getScenario().getName();
                    List<Step> steps = scenarioResult.getScenario().getSteps();
                    final String[] path = {""};
                    steps.forEach(step -> {
                        if (step.toString().toLowerCase().contains("and path") || step.toString().toLowerCase().contains("* path")) {
                            path[0] = step.toString().replaceAll("(.*path |'| |\"|\\+.*\\.|\\+)", "").replaceAll(",.*\\.", "/");
                        }
                    });
                    error.setValue(":heavy_multiplication_x: " + feature + " - " + scenario + " (`" + path[0] + "`)");
                    error.setValueShortEnough(false);

                    blocks.add(error);
                }
            }
        }

        return blocks;
    }

    private static String getGitlabArtifactUrl(String jobUrl) {
        try {
            URL artifactUrl = new URL(jobUrl + "/artifacts/download");
            return artifactUrl.toString();

        } catch (MalformedURLException error) {
            log.error("Could not get Gitlab Job Artifact URL. Error: " + error.toString());
            return null;
        }
    }

    private static String getGitlabJobUrl() {
        try {
            String projectUrl = System.getenv("CI_PROJECT_URL");
            String jobId = System.getenv("CI_JOB_ID");

            log.info("CI_PROJECT_URL: " + projectUrl);
            log.info("CI_JOB_ID: " + jobId);

            URL jobUrl = new URL(projectUrl + "/-/jobs/" + jobId);
            return jobUrl.toString();

        } catch (MalformedURLException error) {
            log.error("Could not get Gitlab URL. Error: " + error.toString());
            return null;
        }
    }

    private static String getColor(boolean success) {
        return success ?
                "#36a64f" :
                "#ff241d";
    }

    private static String getTitle(Results results, String tags, String env) {
        return "*Test Result:* " + results.getPassCount() + "/" + results.getScenarioCount()
                + "\n*Environment:* " + env
                + "\n*Tags:* " + tags;
    }
}