FROM maven:3.6.3-openjdk-8

# Create app directory
WORKDIR /usr/src/karate-

COPY . .

RUN mvn clean install -DskipTests